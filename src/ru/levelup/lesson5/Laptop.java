package ru.levelup.lesson5;

import java.util.Objects;

public class Laptop {

    private String brand;
    // private Phone phone; -> phone.equals(second.phone);
    private int ram;
    private double cpu;

    public Laptop(String brand, int ram, double cpu) {
        this.brand = brand;
        this.ram = ram;
        this.cpu = cpu;
    }

    // l1.equals(l2)
    // l1 - this
    // l2 - o

    // 1 var
    // Laptop - l11
    // PersonalLaptop extend Laptop - l12
    //  l11.equals(l12) -> true - l12 instanceof Laptop -> true
    //  l12.equals(l11) -> false - l11 instanceof PersonalLaptop -> false

    // 2 var
    // Laptop - l11
    // PersonalLaptop extend Laptop - l12
    //  l11.equals(l12) -> false -> Laptop != PersonalLaptop
    //  l12.equals(l11) -> false -> PersonalLaptop != Laptop

    @Override
    public boolean equals(Object o) {
        if (this == o) return true; // повторяет логику метода equals из Object

        // 1 var
        // instanceof
        // <obj> instanceof <Class> -> true, если obj относится к классу Class, false если нет или если obj null
        // if (!(o instanceof Laptop)) return false;

        // 2 var
        if (o == null || getClass() != o.getClass()) return false;

        Laptop second = (Laptop) o;                         // ClassCastException - fixed
        return ram == second.ram &&                         // NullPointerException (NPE) - fixed
                Objects.equals(brand, second.brand);        // NullPointerException (NPE) - fixed
    }

    @Override
    public int hashCode() {
//        int result = 13;
//        result += 31 * result + ram;
//        result += 31 * result + brand.hashCode();
//        return result;
        return Objects.hash(ram, brand);
    }

}
