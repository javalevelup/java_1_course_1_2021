package ru.levelup.lesson5;

import ru.levelup.lesson4.hw.Phone;

@SuppressWarnings("ALL")
public class ComparisonApp {

    public static void main(String[] args) {
        Laptop l1 = new Laptop("Lenovo", 8, 2.8);
        Laptop l2 = new Laptop("Lenovo", 8, 2.8);
        Laptop l4 = new Laptop(null, 8, 2.8);
        // l4.equals(l1);
        Laptop l3 = l1; // == вернет true, так как l1 и l3 ссылаются на одну и ту же область памяти

        boolean isEqual = l1 == l2; // == - сравнивает ссылки
        System.out.println(isEqual);

        boolean equals = l1.equals(l2);
        // boolean equals = l1.equals(null);
        System.out.println(equals);

        System.out.println(l2.getClass());

        // Phone p = new Phone(null, null, 3);
        // l1.equals(p);

    }

}
