package ru.levelup.lesson5;

// Модификаторы доступа (access modifiers)
// private - доступ только внутри класса
// default-package (private-package) - отсутствие модификатора доступа - доступ внутри класса и внутри пакета
// protected - доступ внутри класса, внутри пакета и внутри наследника
// public - нет ограничений доступа
public class TriplePoint {

    // C++
    // private:
    //  int x;
    //  int y;

    private int x;
    private int y;
    private int z;

    public void changeXCoordinate(int x) {
        if (checkInterval(x)) {
            this.x = x;
        }
    }

    // getter
    // <тип поля> get<Имя_поля>() { return <поле>; }
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    // setter
    // void set<Имя_поля>(<тип поля> <имя поля>) { this.<поле> = <поле>; }
    public void setX(int x) {
        this.x = x;
    }

    private boolean checkInterval(int value) {
        return value > 0;
    }

    public double calculateDistance(TriplePoint p2) {
        double k1 = Math.abs(this.x - p2.x);
        double k2 = Math.abs(this.y - p2.y);

        return Math.sqrt(k1 * k1 + k2 * k2);
    }

}
