package ru.levelup.lesson5.hw;

public class HouseFloorFilter extends BaseFilter {

    private int floorBound;

    public HouseFloorFilter(int floorBound) {
        this.floorBound = floorBound;
    }

    @Override
    public boolean filter(Object value) {
        // тут есть проверки типов (instanceof House)
        House house = (House) value;
        return house.getFloors() > floorBound;
    }

}
