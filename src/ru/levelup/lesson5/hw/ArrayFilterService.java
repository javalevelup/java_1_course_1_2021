package ru.levelup.lesson5.hw;

public class ArrayFilterService {

    // Object[] - массив со значениями, который нужно будет отфильтровать
    // BaseFilter arrayFilter - по какому принципе будут отфильтрованы значения

    // House[] houses
    public Object[] filterArray(Object[] objects, BaseFilter arrayFilter) {
        for (int i = 0; i < objects.length; i++) {
            // objects[i]
            if (!arrayFilter.filter(objects[i])) {
                // h1, h2, h3, h4
                // null, h2, null, h4
                // h2, h4, null, null
                // h2, h4
                objects[i] = null; // objects[i] больше не существует, память будет освобождена
            }
        }

        return objects;
    }

    // House[] houses;
    // House[] filteredHouses;
    // House[] filtered = (House[]) filterArray(houses, filteredHouses, arrayFilter);
    public Object[] filterArray(Object[] objects, Object[] resultArray, BaseFilter arrayFilter) {
        return resultArray;
    }

}
