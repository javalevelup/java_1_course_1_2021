package ru.levelup.lesson5.hw;

public class House {

    private int floors;
    private double height;

    public House(int floors, double height) {
        this.floors = floors;
        this.height = height;
    }

    public int getFloors() {
        return floors;
    }

    public void setFloors(int floors) {
        this.floors = floors;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
