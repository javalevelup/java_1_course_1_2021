package ru.levelup.lesson5;

public class PointDistanceCalculator {

    public double calculateDistance(TriplePoint p1, TriplePoint p2) {
        double k1 = Math.abs(p1.getX() - p2.getX());
        double k2 = Math.abs(p1.getY() - p2.getY());

        return Math.sqrt(k1 * k1 + k2 * k2);
    }

}
