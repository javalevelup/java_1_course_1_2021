package ru.levelup.lesson5;

public class App {

    public static void main(String[] args) {

        TriplePoint tp = new TriplePoint();
        tp.changeXCoordinate(5);
        // tp.setX(43);
        System.out.println("point.x: " + tp.getX());

        TriplePoint tp2 = new TriplePoint();

        PointDistanceCalculator calculator = new PointDistanceCalculator();
        calculator.calculateDistance(tp, tp2);

        tp.calculateDistance(tp2);

    }

}
