package ru.levelup.lesson3;

@SuppressWarnings("ALL")
public class PrimitiveVsReferences {

    // pass-by-value
    public static void main(String[] args) {
        int value = 10; // ac3
        Point point = new Point(); // 32a
        point.x = 10; // 32a.4c = 10

        changePrimitive(value);
        changePointCoordinate(point);

        System.out.println(value);      // 20/20/10 - no -> 10 - ac3
        System.out.println(point.x);    // 10/20/10 - no -> 20 - 32a.4c
    }

    static void changePrimitive(int v) { // v - 53a
        v = 20; //
    }

    static void changePointCoordinate(Point point) { // point - 32a
        point.x = 20;  // 32a.4c = 20
    }

}
