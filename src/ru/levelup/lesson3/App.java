package ru.levelup.lesson3;

public class App {

    public static void main(String[] args) {
        // int[] xArray;
        // int[] yArray;
        // int[] zArray;

        // Point[] pointArray;

        // переменная - это переменная примитивного типа
        // переменная ссылочного типа (непримитивного типа) - это объект
        // p1 - объект, ссылка, экземпляр, object, reference, instance
        // p1 - 145ab564c58
        Point p1 = new Point(); // объявили переменную p1, тип которой - Point - объект
        p1.x = 8;
        p1.y = 3;
        int sum = p1.x + p1.y;
        System.out.println("Sum: " + sum);

        // p2 - 75bf4a4c4c
        Point p2 = new Point(); // new - НОВЫЙ объект, новая (другая) область памяти
        p2.x = 5;
        p2.y = 12;

        p2.changeCoordinates(-7, 1);
        p1.changeCoordinates(9, -9);

        p1.setFields(3, 2);
        p1.setFields(3, 3, "A");

        // p3 - 536abcd3da
        Point p3 = new Point(6, -2, "C");

//        Point p4 = null; // - null
//        p4.changeCoordinates(1, 2);
//        p4.x = 4;
    }

}
