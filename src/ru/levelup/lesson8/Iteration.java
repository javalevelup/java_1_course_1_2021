package ru.levelup.lesson8;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Iteration {

    public static void main(String[] args) {
        List<Double> prices = new ArrayList<>();

        prices.add(54.33);
        prices.add(7.65);
        prices.add(986.45);
        prices.add(137.54);
        prices.add(76.43);



        System.out.println();

        Iterator<Double> iter = prices.iterator();
        // 1iter: next() will return 54.33
        // 2iter: next() will return 7.65
        while (iter.hasNext()) {
            Double el = iter.next();
            System.out.println(el);
            if (el > 100) {
                iter.remove();
            }
        }

        System.out.println();
        for (Double price : prices) {
            System.out.println(price);
        }
    }

}
