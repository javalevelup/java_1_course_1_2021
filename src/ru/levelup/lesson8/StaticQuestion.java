package ru.levelup.lesson8;

@SuppressWarnings("ALL")
public class StaticQuestion {

    public static void main(String[] args) {
        StaticQuestion o = null;
        o.printHello(); // -> StaticQuestion.printHello();

        // obj.staticMethod(); -> Class.staticMethod();
    }

    static void printHello() {
        System.out.println("Hello!");
    }

}
