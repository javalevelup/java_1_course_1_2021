package ru.levelup.lesson8;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Bank {

    static double initialAmount = 10.0;

    // String - accountId
    // Double - amount
    private Map<String, Double> accounts = new HashMap<>();

    public String openAccount() {
        String accountId = UUID.randomUUID().toString();
        accounts.put(accountId, initialAmount);

        return accountId;
    }

    public static void changeInitialAmount(double amount) {
        if (amount >= 0) {
            initialAmount = amount;
            // openAccount();
            // Bank b = new Bank();
            // b.accounts = new HashMap<>();
        }
    }

}
