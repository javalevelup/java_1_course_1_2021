package ru.levelup.lesson8;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HashMapExample {

    public static void main(String[] args) {

        // HashSet, LinkedHashSet, TreeSet
        Map<String, Double> prices = new HashMap<>();

        prices.put("Молоко", 76.4);
        prices.put("Сыр", 653.3);
        prices.put("Йогурт", 46.23);
        prices.put("Хлеб", 52.34);
        // 4 пары в Map ^^

        double milkPrice = prices.get("Молоко");
        System.out.println(milkPrice);

        // keySet() - множество ключей
        // values() - коллекция всех значений
        // entrySet() - множество entry

        System.out.println();
        Set<String> keys = prices.keySet();
        for (String key : keys) {
            Double value = prices.get(key);
            System.out.println("Key: " + key + ", value: " + value);
        }

        System.out.println();
        Set<Map.Entry<String, Double>> entries = prices.entrySet();
        for (Map.Entry<String, Double> entry : entries) {
            System.out.println("Key: " + entry.getKey() + ", value: " + entry.getValue());
        }

    }

}
