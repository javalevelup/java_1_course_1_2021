package ru.levelup.lesson8;

import java.util.ArrayList;
import java.util.List;

public class CollectionRemove {

    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(1);
        numbers.add(2);
        numbers.add(4);
        numbers.add(6);
        numbers.add(7);

        // int index = 5;
        // numbers.remove(index); -- удаление по индексу

        // byte b = 4;
        // numbers.remove(b); -- удаление по индексу

        // Integer i = 5;
        // numbers.remove(i); -- удаление по значению

        //      remove(Integer value);
        //      remove(int index);
        numbers.remove(5);
        numbers.remove(Integer.valueOf(5));


    }

}
