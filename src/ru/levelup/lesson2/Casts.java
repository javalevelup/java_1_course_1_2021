package ru.levelup.lesson2;

public class Casts {

    // psvm + tab
    public static void main(String[] args) {
        int intVar = 345;
        long longFromInt = intVar; // неявное расширяющее преобразование
        // int intFromLong = longFromInt; // compile error
        int intFromLong = (int) longFromInt; // явное сужающее преобразование

        int var = 130;
        byte b = (byte) var; // -126

        // intVar = intVar + 1;

        byte a = 1;
        byte c = 1;
        byte d = (byte)(a + c);

        a += 10; // a = a + 10
        a += b + d; // a = a + (b + d);

    }

}
