package ru.levelup.lesson2;

import java.util.Random;

public class Arrays {

    public static void main(String[] args) {

        double p1Price = 4532.43; // цена первого товара
        double p2Price = 4266.44;
        double p3Price = 6465.54;
        double p4Price = 9873.34;

        double avg = (p1Price + p2Price + p3Price + p4Price) / 4;


        // Массив переменных типа double
        // Максимальное количество элементов в массиве = 2147483647
        double[] prices = new double[4];
        // записать значение в массив по индексу
        prices[0] = p1Price;
        prices[1] = p2Price;
        prices[2] = p3Price;
        prices[3] = p4Price;

        // Чтение значение элемента массива по индексу
        System.out.println(prices[2]);

        // Вывести весь массив на экран
        System.out.println();
        int length = prices.length;
        for (int idx = 0; idx < length; idx++) {
            System.out.println(prices[idx]);
        }

        double sum = 0;
        for (int idx = 0; idx < prices.length; idx++) {
            sum += prices[idx];
        }
        double average = sum / prices.length;
        System.out.println("Average: " + average);

        // Заполним массив псевдо случайными значениями
        int[] intArray = new int[20];
        Random r = new Random();
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = r.nextInt(10); // r.nextInt ~ r.generateInt()
            System.out.println(intArray[i]);
        }

    }

}
