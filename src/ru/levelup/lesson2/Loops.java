package ru.levelup.lesson2;

@SuppressWarnings("ALL")
public class Loops {

    public static void main(String[] args) {

        // increment/decrement
        // постфиксный/префиксный
        // i++/++i ~ i = i + 1 ~ i += 1
        // i--/--i ~ i = i - 1 ~ i -= 1
        int i = 10;
        System.out.println(i++); // 10
        System.out.println(++i); // 12

        // sout(i)    // 10
        // i = i + 1; // 11
        // i = i + 1; // 12
        // sout(i)    // 12

        int a = 10;
        int b = 10;
        int c = a++ + ++b;
        // b = b + 1;
        // c = a + b;
        // a = a + 1;
        System.out.println(c);


        for (int counter = 0; counter < 7; counter += 2) {
            System.out.println("I like for loop :) " + counter);
        }

        // for (;;) {} - без счетчика, без условия и без обновления - бесконечный цикл
        // int idx = 0;
        // for (; idx < 100;) { idx = calculateNextIndex(); }

        int idx = 0;
        while (idx < 10) {
            idx += 2;
            System.out.println("Idx: " + idx);
        }

    }

}
