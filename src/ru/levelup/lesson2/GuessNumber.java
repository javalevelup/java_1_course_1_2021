package ru.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

@SuppressWarnings("ALL")
public class GuessNumber {

    public static void main(String[] args) {

        // 1. Считывание с клавиатуры
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число:");
        int number = sc.nextInt(); // nextInt() позволит считать число введенное с клавиатуры

        // 2. Генерация псевдо случайных чисел
        Random r = new Random();
        // nextInt
        // [0, bound)
        // nextInt(5) -> [0, 5) - не включая пять
        // [10, 21)
        // nextInt(11) + 10; -> [0, 11) + 10 -> [10, 21)
        int secretNumber = r.nextInt(5);

        if (number > secretNumber) {
            int localVar = 405;
            System.out.println("Число больше, чем секретное");
        } else {
            int localVar = 3;
            if (number == secretNumber) {
                System.out.println("Числа совпадают");
            } else {
                System.out.println("Число меньше, чем секретное");
            }
        }

        // if..else if..
        // 1 var
        if (number > secretNumber) {
            System.out.println("Число больше, чем секретное");
        } else if (number == secretNumber) {
            System.out.println("Числа совпадают");
        } else {
            System.out.println("Число меньше, чем секретное");
        }

        // 2 var
        if (number > secretNumber) {
            System.out.println("Число больше, чем секретное");
        }
        if (number < secretNumber) {
            System.out.println("Число меньше, чем секретное");
        }
        if (number == secretNumber) {
            System.out.println("Числа совпадают");
        }


    }

}
