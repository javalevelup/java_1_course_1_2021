package ru.levelup.lesson10;

public class ConsoleMenu {

    public void displayMainMenu() {
        System.out.println("Вы находитесь в основном меню. Выберите действие:");
        System.out.println("1. Отобразить список факультетов");
        System.out.println("2. Создать новый факультет");
        System.out.println("0. Завершить программу");
    }

}
