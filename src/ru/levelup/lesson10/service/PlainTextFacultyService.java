package ru.levelup.lesson10.service;

import ru.levelup.lesson10.domain.Faculty;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class PlainTextFacultyService implements FacultyService {

    // static final field - constant
    private static final String FACULTY_FILE_PATH = "faculties.txt";

    @Override
    public Collection<Faculty> loadFacultyList() {
        try (BufferedReader fileReader = new BufferedReader(new FileReader(FACULTY_FILE_PATH))) {

            String line;
            Collection<Faculty> faculties = new ArrayList<>();
            while ( (line = fileReader.readLine()) != null ) {
                Faculty faculty = parseRawString(line);
                if (faculty != null) {
                    faculties.add(faculty);
                }
            }

            return faculties;
        } catch (IOException exc) {
            System.out.println("Ошибка при чтении файла " + FACULTY_FILE_PATH);
            return Collections.emptyList();
        }
    }

    @Override
    public Faculty createFaculty(int facultyId, String name) {
        try (BufferedWriter fileWriter = new BufferedWriter(new FileWriter(FACULTY_FILE_PATH, true))) {

            // ID%%NAME
            String line = facultyId + "%%" + name;
            fileWriter.newLine();
            fileWriter.write(line);
            fileWriter.flush();

            return new Faculty(facultyId, name);
        } catch (IOException exc) {
            System.out.println("Ошибка при записи в файл " + FACULTY_FILE_PATH);
            return null;
        }
    }

    private Faculty parseRawString(String rawFacultyString) {
        // ID%%Name
        // 1%%IT
        if (rawFacultyString.trim().isEmpty()) { // "  ewwe ".trim() -> "ewwe", "   " <- blank
            return null;
        }

        String[] elements = rawFacultyString.split("%%"); // ["1", "IT"]
        return new Faculty(
                Integer.parseInt(elements[0]),
                elements[1]
        );
    }

}
