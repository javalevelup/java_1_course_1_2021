package ru.levelup.lesson10.command;

// 1. Может ли enum наследоваться от другого класса? Нет
// 2. Может ли enum наследоваться от другого enum? Нет
// 3. Может ли enum наследоваться от абстрактного класса? Нет
// 4. Может ли enum реализовывать интерфейс? Да
// 5. Может ли enum иметь абстрактные методы? Да
public enum ActionType {

    // new Actions(1) { } <- анонимный внутренний класс 1
    // new Actions(1) { } <- анонимный внутренний класс 2
    DISPLAY_FACULTIES(1),
    CREATE_FACULTY(2);

    private final int actionIndex;

    ActionType(int actionIndex) {
        this.actionIndex = actionIndex;
    }

    public int getActionIndex() {
        return actionIndex;
    }

    // public abstract void doAction();
}
