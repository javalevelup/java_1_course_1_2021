package ru.levelup.lesson4;

// subclass
public class Rectangle extends Shape {

    Rectangle() {
        // super(new int[0]);
        // super(); // вызов конструктора суперкласса
        System.out.println("Rectangle constructor invoked...");
    }

    Rectangle(int width, int height) {
        // super(); // вызовем коструктор без параметров
        super(new int[] {width, height, width, height });
    }

    Rectangle(int[] sides) {
        super(sides);
    }

    double calculateSquare() {
        return sides[0] * sides[1];
    }

}
