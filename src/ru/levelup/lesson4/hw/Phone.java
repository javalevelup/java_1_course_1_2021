package ru.levelup.lesson4.hw;


public class Phone {

    String number;
    String model;
    double weight;

    String pair;

    Phone() {}

    Phone(String number, String model) {
        this(number, model, 0.0); // вызвали конструктор Phone(String, String)
    }

    public Phone(String number, String model, double weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    }

    void sendMessages(String[] numbers, String message) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println("Send message " + message + " on number " + numbers[i]);
        }
    }

}
