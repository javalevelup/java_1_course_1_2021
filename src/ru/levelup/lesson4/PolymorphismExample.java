package ru.levelup.lesson4;

public class PolymorphismExample {

    public static void main(String[] args) {

        Rectangle r1 = new Rectangle(4, 6);
        Rectangle r2 = new Rectangle(9, 8);
        Triangle t1 = new Triangle();
        t1.sides = new int[] { 6, 9, 12};

        Shape s = new Shape();
        s.sides = new int[] {9, 6, 3, 5, 3};

        // Rectangle r3 = new Rectangle(7, 4);
        // Shape rs = r3;
        Shape rs = new Rectangle(7, 4);

        Shape[] shapes = new Shape[5];
        // shapes[1] = 5;
        shapes[0] = r1; // shapes[0] = (Shape) r1;
        shapes[1] = r2; // shapes[1] = (Shape) r2;
        shapes[2] = t1;
        shapes[3] = s;
        shapes[4] = rs;

        double avg = avgPerimeter(shapes);
        System.out.println("Avg  perimeter: " + avg);
    }

    static double avgPerimeter(Shape[] shapes) {
        double sum = 0;
        for (int i = 0; i < shapes.length; i++) {
            sum += shapes[i].calculatePerimeter();
        }

        return sum / shapes.length;
    }

}
