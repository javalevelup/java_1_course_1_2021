package ru.levelup.lesson4;

@SuppressWarnings("ALL")
public class ObjectCast {

    public static void main(String[] args) {
        Rectangle r = new Rectangle();
        Shape s = r;
        Rectangle rs = (Rectangle) s;

        Object or = r;
        Object os = s;

        Shape so = (Shape) or;

        Shape shape = new Shape();
        shape.sides = new int[] {4, 6, 7, 8, 8};

        // Triangle t1 = (Triangle) shape; // ClassCastException
        Object o = new Object();
        // Shape shapeo = (Shape) o;

        // r.calculateSquare();

        Triangle t = new Triangle();
        t.sides = new int[] { 1, 3, 5};

        Shape st = t;
        double p = st.calculatePerimeter();
        System.out.println("Perimeter shape: " + p);

    }

}
