package ru.levelup.lesson1;

// Полное имя класса - имя пакета (полный путь по папкам) + имя класса
// Полное имя класса - ru.levelup.lesson1.HelloWorld
public class HelloWorld {

    // Точка входа
    public static void main(String[] args) {
        System.out.println("Hello world!"); // Вывести на экран (в консоль) строчку "Hello world!"


        // 0 - 00000000
        // 1 - 00000001
        // 2 - 00000010
        // 3 - 00000011
        // 4 - 00000100
        // 0 0100101 - 1 * 2^0 + 0 * 2^1 + 1 * 2^2 + 0 * 2^3 + 0 * 2^4 + 1 * 2^5 = 1 + 4 + 32 = 37
        // 0 1111111 - 1 + 2 + 4 + 16 + 32 + 64 = 127
        // 127 - 1111111
        // -128 - 1 000000

        // переменная
        // <тип переменной> <имя переменной>
        int first; // объявили переменную

        // запись значение в переменную
        first = 583;

        int second = 7483; // инициализация переменной
        // second = 2345;

        int result = second * first;
        System.out.println(result);

        int sum;
        sum = result + second;
        // "String" + "anotherString" -> "StringanotherString"
        // "String" + <любой тип> -> Строка
        // "String" + <тип преобразуется в строку> -> "String" + "Значение" -> "StringЗначение"
        System.out.println("Sum = " + sum);

        int a = 10;
        int b = 10;
        System.out.println(a + b + " = sum(a,b)"); // 20 = sum(a,b)
        System.out.println("sum(a,b) = " + a + b); // sum(a,b) = 1010 -> "sum(a,b) = 10" + b -> "sum(a,b) = 1010"
        System.out.println("sum(a,b) = " + (a + b));

    }

}
