package ru.levelup.lesson6;

// АК - могут содержать абстрактные методы
// Нельзя создать объект АК
// АК может иметь обычные поля/методы
// Наследки абстрактного класса будет обязан переопределить ВСЕ абстрактные методы
public abstract class ObjectComparator {

    // < 0 - first < second
    // 0 - first = second
    // > 0 - first > second
    protected abstract int compare(Object first, Object second); // объявили метод

    public int compareTwoRealObjects(Object first, Object second) {
        if (first == second) return 0; // null == null -> true -> return 0;
        if (first == null) return -1;
        if (second == null) return 1;

        return compare(first, second);
    }

}
