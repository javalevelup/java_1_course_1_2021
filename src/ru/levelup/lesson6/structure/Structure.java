package ru.levelup.lesson6.structure;

public interface Structure extends Iterable<Integer> {

    void addValue(int value);

    void removeByValue(int value);

    void trim();

}
