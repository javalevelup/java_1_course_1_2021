package ru.levelup.lesson6.structure;

import java.util.Iterator;

public class StructureApp {

    public static void main(String[] args) {
        Structure s = new DynamicArray(3);
        // Structure s = new OneWayList();
        s.addValue(40);
        s.addValue(65);
        s.addValue(32);
        s.addValue(54);
        s.addValue(67);
        s.addValue(32);
        s.addValue(123);

        s.removeByValue(32);
        System.out.println();

        Iterator<Integer> iter = s.iterator();
        while (iter.hasNext()) {
            Integer el = iter.next();
            System.out.println(el);
        }

        for (Integer value : s) {
            System.out.println(value);
        }
    }

}
