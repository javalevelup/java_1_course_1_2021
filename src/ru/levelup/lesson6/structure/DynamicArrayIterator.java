package ru.levelup.lesson6.structure;

import java.util.Iterator;

public class DynamicArrayIterator implements Iterator<Integer> {

    private int[] elements;
    private int currentIndex;
    private int elementsCount;

    public DynamicArrayIterator(int[] elements, int elementsCount) {
        this.elements = elements;
        this.elementsCount = elementsCount;
    }

    @Override
    public boolean hasNext() {
        return currentIndex < elementsCount;
    }

    @Override
    public Integer next() {
//        return elements[currentIndex++];
        int el = elements[currentIndex];
        currentIndex++;
        return el;
    }

}
