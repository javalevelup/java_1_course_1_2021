package ru.levelup.lesson6.structure;

import java.util.Iterator;

// Динамический массив - список на основе массива
//  список - набор однотипных элементов
//
public class DynamicArray implements Structure {

    private int[] elements;
    private int size;       // 1. Количество элементов в массиве (стуктуре)
                            // 2. Индекс куда вставлять следующий элемент

    public DynamicArray(int initialCapacity) {
        this.elements = new int[initialCapacity];
        this.size = 0;
    }

    // Вставка в конец
    //  [0, 0, 0]
    //  [14, 43, 65, 5, 0]
    @Override
    public void addValue(int value) {
        if (elements.length == size) {
            int[] oldArray = elements;
            elements = new int[size * 2];
            // oldArray -> elements
            System.arraycopy(oldArray, 0, elements, 0, oldArray.length);
        }

        elements[size++] = value;
        // elements[size] = value;
        // size = size + 1;
    }

    @Override
    public void removeByValue(int value) {
        // [1, 3, 5, 6, 0, 0, 0, 0] length = 8, size = 4
        // [1, 5, 6, 0, 0, 0, 0, 0] length = 8, size = 3

        // addValue(7);
        // [1, 5, 6, 7, 0, 0, 0, 0]
        // shift();
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == value) {
                // array shift - сдвиг элементов влево
                // [1, 5, 6, 0, 0, 0, 0, 0]  i = 0, length = 8 - 0 - 1 = 7
                // [1, 5, 6, 0, 0, 0, 0, 0]  i = 1, length = 8 - 1 - 1 = 6
                System.arraycopy(elements, i + 1, elements, i, elements.length - i - 1);
                size--;
            }
        }
    }

    @Override
    public void trim() {
        // [1, 5, 3, 2, 0, 0, 0, 0].trim() -> [1, 5, 3, 2]
        // trim() -> length = size
        if (size != elements.length) {
            int[] oldArray = elements;
            elements = new int[size];
            System.arraycopy(oldArray, 0, elements, 0, size);
        }
    }

    // класс должен имплементировать интерфейс Iterable
    //  - у класса появляется метод iterator, который возвращает итератор для класса
    //  - итератор - то, как мы будем итерироваться по классу
    @Override
    public Iterator<Integer> iterator() {
        return new DynamicArrayIterator(elements, size);
    }

}
