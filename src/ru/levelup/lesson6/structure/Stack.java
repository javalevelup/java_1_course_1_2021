package ru.levelup.lesson6.structure;

public class Stack {

    private int[] array;
    private int topIndex; // куда вставлять следующию элемент
                          // откуда забирать элемент
                          // количество элементов в стеке

    public Stack(int capacity) {
        this.array = new int[capacity];
    }

    public void push(int value) {
        if (topIndex != array.length) {
            array[topIndex++] = value;
        }
    }

    public int pop() {
        if (topIndex > 0) {
            // [1, 4, 5, 6, 0, 0] topIndex = 4
            // --topIndex;
            // return array[topIndex];

            // [1, 4, 5, 6, 0, 0] --topIndex -> 3
            // [1, 4, 5, 6, 0, 0] return array[3] -> 6
            // push(9); topIndex = 3
            // [1, 4, 5, 9, 0, 0]
            return array[--topIndex];
        }
        return 0;
    }

}
