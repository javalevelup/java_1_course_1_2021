package ru.levelup.lesson6.structure;

import java.util.Iterator;

public class OneWayList implements Structure {

    private ListElement head;
    private int size;

    @Override
    public void addValue(int value) {
        ListElement el = new ListElement(value);
        if (head == null) { // это происходит когда список пустой
            head = el;
        } else {
            ListElement currentPointer = head;
            while (currentPointer.getNext() != null) {
                currentPointer = currentPointer.getNext();
            }
            // currentPointer - ссылка на последний элемент списка
            currentPointer.setNext(el);
        }
        size++;
    }

    @Override
    public void removeByValue(int value) {
        if (size == 1) {
            head = null;
            return;  // завершение метода
        }

        if (head.getValue() == value) {
            head = head.getNext();
            return;
        }

        // 4, 5, 6, 5, 7
        // removeByValue(5); -> 4, 6, 5, 7

        ListElement current = head;
        ListElement previous = head;

        while (current != null) {
            if (current.getValue() == value) {
                previous.setNext(current.getNext());
                size--;
            } else {
                previous = current;
            }
            current = current.getNext();
        }

//        while (current != null && current.getValue() != value) {
//            previous = current; // previous -> head = head;
//            current = current.getNext();
//        }
//
//        if (current != null) {
//            previous.setNext(current.getNext());
//        }
//        size--;
    }

    @Override
    public void trim() {

    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }

}
