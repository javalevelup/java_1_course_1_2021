package ru.levelup.lesson7;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CollectionExample {

    public static void main(String[] args) {

        List<String> productNames = new ArrayList<>(); // ArrayList ~ DynamicArray
        productNames.add("Мясо");
        productNames.add("Сметана");
        productNames.add("Молоко");
        productNames.add("Яйца");
        productNames.add("Печенье");

        // foreach
        // for (<Generic Type/Array type> <variable name> : <collection/array>)

//        int[] array = { 1, 4, 6, 7 };
//        for (int i = 0; i < array.length; i++) {
//            System.out.println(array[i]);
//        }
//        for (int el : array) {
//            System.out.println(el);
//        }

        for (String name : productNames) {
            System.out.println(name);
        }

        System.out.println();
        int eggsIndex = productNames.indexOf("Яйца");
        System.out.println("Egg's index: " + eggsIndex);

        int doesNotExist = productNames.indexOf("Курица");
        System.out.println("Does not exist: " + doesNotExist);

        System.out.println();

        productNames.add(3, "Петрушка");
        for (String name : productNames) {
            System.out.println(name);
        }

    }

}
