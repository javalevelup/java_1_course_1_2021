package ru.levelup.lesson7;

@SuppressWarnings("ALL")
public class TypeWrapper {

    public static void main(String[] args) {
        int i = 54;
        Integer integer = 341;

        double d = 757.3;
        Double doubleObject = 642.43;

        char c = '3';
        Character character = '3';

        // boxing - упаковка : autoboxing
        // unboxing - распаковка : autounboxing

        int intValue = 5334;
        Integer integerValue = Integer.valueOf(intValue); // упаковка (из примитива в объект)
        int fromObjectValue = integerValue.intValue(); // распаковка (из объекта в примитив)

        Integer i1 = 127;
        Integer i2 = 127;
        Integer i3 = 129;
        Integer i4 = 129;

        System.out.println(i1 == i2); // true
        System.out.println(i3 == i4); // false
    }

}
