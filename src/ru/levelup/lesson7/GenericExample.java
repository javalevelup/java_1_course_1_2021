package ru.levelup.lesson7;

public class GenericExample {

    public static void main(String[] args) {
        GenericClass<String> stringGenericClass = new GenericClass<>();
        stringGenericClass.changeField("some string");
        // stringGenericClass.changeField(54);
        String value = stringGenericClass.getField();
        System.out.println(value);

        GenericClass<Integer> integerGenericClass = new GenericClass<>();
        integerGenericClass.changeField(54);
        // integerGenericClass.changeField("string");

    }

}
