package ru.levelup.lesson9;

@SuppressWarnings("ALL")
public class TryFinally {

    public static void main(String[] args) {
        int v = getNumber();
        System.out.println(v);
    }

    public static int getNumber() {
        try {
            System.out.println("try");
            // System.exit(0);
            // return 1;
            // throw new RuntimeException();
        } catch (Exception exc) {
            System.out.println("catch");
            return 2;
        } finally {
            System.out.println("finally");
            return 3;
        }
    }

}
