package ru.levelup.lesson9;

import java.io.File;
import java.io.IOException;

// /Users/dmitryprotsko/IdeaProjects/other/java_1_course_1_2021/src/ru/levelup/lesson9/FileExample.java - absolute
// FileExample.java
// ../../lesson9/FileExample.java
@SuppressWarnings("ALL")
public class FileExample {

    public static void main(String[] args) throws IOException {
        File testFile = new File("test.txt");
        boolean isNull = testFile == null;
        System.out.println("isNull: " + isNull);

        boolean isExist = testFile.exists();
        System.out.println("isExist: " + isExist);

        boolean isCreated = testFile.createNewFile();
        System.out.println("isCreated: " + isCreated);

        File testDir = new File("test/");

        boolean isDirExist = testDir.exists();
        System.out.println("isDirExist: " + isDirExist);

        boolean isDirCreated = testDir.mkdir();
        System.out.println("isDirCreated: " + isDirCreated);

        boolean isDir = testDir.isDirectory();
        System.out.println("isDir: " + isDir);

    }

}
