package ru.levelup.lesson9;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("ALL")
public class DateFormatter {

    public Date transformToDate(String value) {
        // d - 4
        // dd - 04

        // hh - 12
        // HH - 24
        // S - milliseconds
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            Date date = formatter.parse(value);
            return date;
        } catch (ParseException e) {
            // описываем, что делать, в случае если это исключение произошло
            System.out.println("Была передана дата в неправильном формате");
            // return null;
            throw new RuntimeException("Была передана дата в неправильном формате");
        }
    }

}
