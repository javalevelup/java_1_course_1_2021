package ru.levelup.lesson9.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileIO {

    public static void main(String[] args) throws Exception {
        // InputStream
        // OutputStream
        // Reader
        // Writer

        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        String numberAsString = console.readLine();
        int number = Integer.parseInt(numberAsString);


//        catch (NumberFormatException exc) {
//            System.out.println(exc.getMessage());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        // Scanner s = new Scanner(System.in);
        // int number = s.nextInt();


        // console.close() <- закроет System.in поток


        File properties = new File("application.properties");

        try (BufferedReader reader = new BufferedReader(new FileReader(properties))) {

            String line;
            while ( (line = reader.readLine()) != null ) {
                System.out.println(line);
            }

        } catch (IOException exc) {
            System.out.println("Ошибка при чтении файла");
        }


//        BufferedReader reader = null;
//        try {
//            reader = new BufferedReader(new FileReader(properties));
//
//            String line;
//            while ( (line = reader.readLine()) != null ) {
//                System.out.println(line);
//            }
//
//        } catch (IOException exc) {
//            System.out.println("Ошибка при чтении файла");
//        } finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException e) {
//                    System.out.println("Невозможно закрыть файл");
//                }
//            }
//        }

    }

}
